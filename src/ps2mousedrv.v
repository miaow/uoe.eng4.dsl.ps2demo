`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		10:08:45 01/30/2014
// Design Name:		PS/2 Demo
// Module Name:		ps2mousedrv
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 module - complete.
//
// Dependencies:	globals.v, ps2rx.v, ps2tx.v
//
// Revision:
// v1.00 - 201402081909Z - Verified
// v0.02 - 201401301100Z - Implementation done
// v0.01 - 201401301008Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////

`include "globals.v"

module ps2mousedrv
(
    input				CLK,
    input				RESET,
    /* I/O */
    inout				IO_CLK,
    inout				IO_DAT,
    output reg	[3:0]	DAT_STATUS,
    output reg	[7:0]	DAT_X_POS,
    output reg	[6:0]	DAT_Y_POS
);

	/* Mouse position limits - 160x120 screen */
	parameter	[7:0]	DAT_X_POS_max	= 160;
	parameter	[7:0]	DAT_Y_POS_max	= 120;

	/* Tri-state signals (IO_DAT and IO_CLK) */
	/* IO_CLK */
	reg					IO_CLK_IN;
	wire				IO_CLK_OE;
	/* if output enabled, pull IO_CLK low */
	assign IO_CLK						= IO_CLK_OE ? `LOW : `Z;

	/* IO_DAT */
	wire				IO_DAT_IN;
	wire				IO_DAT_OUT;
	wire				IO_DAT_OE;
	/* if output enabled, drive data line, else leave at high impedance */
	assign IO_DAT						= IO_DAT_OE ? IO_DAT_OUT : `Z;
	assign IO_DAT_IN					= IO_DAT;

	/* clock filter - ensures clock is stable before latching data */
	reg	[7:0]		buf_IO_CLK;
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			IO_CLK_IN		<= `LOW;
		end
		else begin
			/* simple shift reg */
			buf_IO_CLK[7:1]	<= buf_IO_CLK[6:0];
			buf_IO_CLK[0]	<= IO_CLK;
			
			/* detect falling edge */
			if( IO_CLK_IN & ( buf_IO_CLK == 8'h00 ) ) begin
				IO_CLK_IN	<= `LOW;
			end
			/* and rising edge */
			if( ~IO_CLK_IN & ( buf_IO_CLK == 8'hFF ) ) begin
				IO_CLK_IN	<= `HIGH;
			end
		end
	end

	/* generate a Tx module instance */
	wire			TX_XMIT;
	wire			TX_XMIT_OK;
	wire	[7:0]	TX_BYTE;
	ps2tx tx
	(
		.CLK(CLK),
		.RESET(RESET),
		/* I/O - CLK */
		.IO_CLK_IN(IO_CLK_IN),
		.IO_CLK_OE(IO_CLK_OE),
		/* I/O - DATA */
		.IO_DAT_IN(IO_DAT_IN),
		.IO_DAT_OUT(IO_DAT_OUT),
		.IO_DAT_OE(IO_DAT_OE),
		/* ctrl */
		.XMIT(TX_XMIT),
		.BYTE(TX_BYTE),
		.XMIT_OK(TX_XMIT_OK)
	);

	/* and an Rx module instance */
	wire			RX_RECV_EN;
	wire	[7:0]	RX_BYTE;
	wire	[1:0]	RX_BYTE_ERR;
	wire			RX_RECV_OK;
	ps2rx rx
	(
		.CLK(CLK),
		.RESET(RESET),
		/* I/O - CLK */
		.IO_CLK_IN(IO_CLK_IN),
		/* I/I - DATA */
		.IO_DAT_IN(IO_DAT_IN),
		/* ctrl */
		.RECV_EN(RX_RECV_EN),
		.BYTE(RX_BYTE),
		.BYTE_ERR(RX_BYTE_ERR),
		.RECV_OK(RX_RECV_OK)
	);

	/* and the state machine */
	wire	[7:0]	DAT_STATUS_raw;
	wire	[7:0]	DAT_DX_raw;
	wire	[7:0]	DAT_DY_raw;
	wire			IRQ;
	ps2ctrl ctrl
	(
		.CLK(CLK),
		.RESET(RESET),
		/* Tx interface */
		.TX_XMIT(TX_XMIT),
		.TX_BYTE(TX_BYTE),
		.TX_XMIT_OK(TX_XMIT_OK),
		/* Rx interface */
		.RX_RECV_EN(RX_RECV_EN),
		.RX_BYTE(RX_BYTE),
		.RX_BYTE_ERR(RX_BYTE_ERR),
		.RX_RECV_OK(RX_RECV_OK),
		/* data */
		.DAT_STATUS(DAT_STATUS_raw),
		.DAT_DX(DAT_DX_raw),
		.DAT_DY(DAT_DY_raw),
		.IRQ(IRQ)
	);

	/* pre-processing - handle OVF and sign; also track actual X/Y pos of mouse
	 */
	wire	signed	[8:0]	DAT_DX;
	wire	signed	[8:0]	DAT_DY;
	wire	signed	[8:0]	next__DAT_X_POS;
	wire	signed	[8:0]	next__DAT_Y_POS;

	/* modify DX and DY to take into account for overflows and direction
	 * 			if( OVF )
	 *			{
	 *				if( NEGATIVE ) -MAX
	 *				else +MAX
	 *			}
	 *			else
	 *				sign:DAT_D*_raw
	 */
	assign DAT_DX = DAT_STATUS_raw[6] ?
		( DAT_STATUS_raw[4] ?
			{DAT_STATUS_raw[4],8'h00} :
			{DAT_STATUS_raw[4],8'hFF} ) :
		{DAT_STATUS_raw[4],DAT_DX_raw[7:0]};
	assign DAT_DY = DAT_STATUS_raw[7] ?
		( DAT_STATUS_raw[5] ?
			{DAT_STATUS_raw[5],8'h00} :
			{DAT_STATUS_raw[5],8'hFF} ) :
		{DAT_STATUS_raw[5],DAT_DY_raw[7:0]};

	/* calculate the mouse position */
	assign next__DAT_X_POS = {1'b0, DAT_X_POS} + DAT_DX;
	/* reverse the movement for Dy to make the directions consistent with VGA
	 * addressing mode.
	 */
	assign next__DAT_Y_POS = {2'b00, DAT_Y_POS} - DAT_DY;

	/* update the o/p registers */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			DAT_STATUS			<= 4'h0;
			DAT_X_POS			<= DAT_X_POS_max/2;
			DAT_Y_POS			<= DAT_Y_POS_max/2;
		end
		else if( IRQ ) begin
			/* strip the status of the redundant info */
			DAT_STATUS			<= DAT_STATUS_raw[3:0];
			
			/* modify X position wrt screen limits */
			if( next__DAT_X_POS < 0 )
				DAT_X_POS		<= 0;
			else if( next__DAT_X_POS > ( DAT_X_POS_max - 1 ) )
				DAT_X_POS		<= DAT_X_POS_max - 1;
			else
				DAT_X_POS		<= next__DAT_X_POS[7:0];
				
			/* modify Y position wrt screen limits */
			if( next__DAT_Y_POS < 0 )
				DAT_Y_POS		<= 0;
			else if( next__DAT_Y_POS > ( DAT_Y_POS_max - 1 ) )
				DAT_Y_POS		<= DAT_Y_POS_max - 1;
			else
				DAT_Y_POS		<= next__DAT_Y_POS[6:0];
		end
	end

endmodule
