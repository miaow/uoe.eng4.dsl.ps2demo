`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
// 
// Create Date:		10:10:52 01/23/2014
// Design Name:		PS2 Demo
// Module Name:		globals
// Project Name:	DSL
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		Global macros to be included in other files
//
// Dependencies:	none
//
// Revision:
// v0.01 - 201401231010Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////

`define TRUE		1'b1
`define FALSE		1'b0
`define HIGH		1'b1
`define LOW			1'b0
`define Z			1'bz
