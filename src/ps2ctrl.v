`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		17:53:58 01/25/2014
// Design Name:		PS/2 Demo
// Module Name:		ps2ctrl
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 controller module.
//
// Dependencies:	ps2tx.v,
//					ps2rx.v,
//					globals.v
//
// Revision:
// v1.00 - 201402081857Z - Verified
// v0.02 - 201401301842Z - Implementation done
// v0.01 - 201401251753Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////

`include "globals.v"

/* TEXT SOURCE: http://www.computer-engineering.org/ps2mouse/
 * Author: Adam Chapweske
 * Last Updated: 04/01/03
 * Legal Information
 * -----------------
 * All information within this article is provided "as is" and without any
 * express or implied warranties, including, without limitation, the implied
 * warranties of merchantibility and fitness for a particular purpose.
 * This article is protected under copyright law. This document may be copied
 * only if the source, author, date, and legal information is included.
 *
 * Following is the set of commands accepted by the standard PS/2 mouse. If the
 * mouse is in stream mode, the host should disable data reporting
 * (command 0xF5) before sending any other commands.
 *
 * 0xFF (Reset) - The mouse responds to this command with "acknowledge" (0xFA)
 * 		then enters reset mode.
 * 0xFE (Resend) - The host sends this command whenever it receives invalid data
 * 		from the mouse. The mouse responds by resending the last packet it sent
 * 		to the host. If the mouse responds to the "Resend" command with another
 * 		invalid packet, the host may either issue another "Resend" command,
 * 		issue an "Error" (0xFC) command, cycle the mouse's power supply to reset
 * 		the mouse, or it may inhibit communication (by bringing the clock line
 * 		low). This command is not buffered, which means "Resend" will never be
 * 		sent in response to the "Resend" command.
 * 0xF6 (Set Defaults) - The mouse responds with "acknowledge" (0xFA) then loads
 * 		the following values: Sampling rate = 100, resolution = 4 counts/mm,
 * 		Scaling = 1:1, data reporting = disabled. The mouse then resets its
 * 		movement counters and enters stream mode.
 * 0xF5 (Disable Data Reporting) - The mouse responds with "acknowledge" (0xFA)
 * 		then disables data reporting and resets its movement counters. This only
 * 		affects data reporting in stream mode and does not disable sampling.
 * 		Disabled stream mode functions the same as remote mode.
 * 0xF4 (Enable Data Reporting) - The mouse responds with "acknowledge" (0xFA)
 * 		then enables data reporting and resets its movement counters. This
 * 		command may be issued while the mouse is in remote mode, but it will
 * 		only affect data reporting in stream mode.
 * 0xF3 (Set Sample Rate) - The mouse responds with "acknowledge" (0xFA) then
 * 		reads one more byte from the host. The mouse saves this byte as the new
 * 		sample rate. After receiving the sample rate, the mouse again responds
 * 		with "acknowledge" (0xFA) and resets its movement counters. Valid sample
 * 		rates are 10, 20, 40, 60, 80, 100, and 200 samples/sec.
 * 0xF2 (Get Device ID) - The mouse responds with "acknowledge" (0xFA) followed
 * 		by its device ID (0x00 for the standard PS/2 mouse). The mouse should
 * 		also reset its movement counters.
 * 0xF0 (Set Remote Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters remote mode.
 * 0xEE (Set Wrap Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters wrap mode.
 * 0xEC (Reset Wrap Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters the mode it was in prior to wrap
 * 		mode (stream mode or remote mode).
 * 0xEB (Read Data) - The mouse responds with "acknowledge" (0xFA) then sends a
 * 		movement data packet. This is the only way to read data in remote mode.
 * 		After the data packet has successfully been sent, the mouse resets its
 * 		movement counters.
 * 0xEA (Set Stream Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters stream mode.
 * 0xE9 (Status Request) - The mouse responds with "acknowledge" (0xFA) then
 * 		sends this 3-byte status packet and resets its movement counters:
 *
 *         Bit 7   Bit 6 Bit 5   Bit 4   Bit 3    Bit 2     Bit 1      Bit 0
 * Byte 1 Always 0 Mode  Enable Scaling Always 0 Left Btn Middle Btn Right Btn
 * Byte 2                             Resolution
 * Byte 3                             Sample Rate
 */

module ps2ctrl
(
    input			CLK,
    input			RESET,
    output			TX_XMIT,
    output	[7:0]	TX_BYTE,
    input			TX_XMIT_OK,
    output			RX_RECV_EN,
    input	[7:0]	RX_BYTE,
    input	[1:0]	RX_BYTE_ERR,
    input			RX_RECV_OK,
    output	[7:0]	DAT_STATUS,
    output	[7:0]	DAT_DX,
    output	[7:0]	DAT_DY,
    output			IRQ /* this should be linked to IRQ12 for BIOS compliance */
);

	/* Setup sequence:
	 * Host FF (reset)
	 * Dev  FA (ACK)
	 * Dev  AA (self-test pass)
	 * Dev  00 (Mouse ID)
	 * Host F4 (enable reporting)
	 * Dev  FA (ACK)
	 * If errors present, start over. Once setup, raise read enable flag.
	 */

	/* State control */
	reg	[3:0]		curr__state, next__state;
	reg	[23:0]		curr__count, next__count;
	/* Tx control */
	reg				curr__TX_XMIT, next__TX_XMIT;
	reg	[7:0]		curr__TX_BYTE, next__TX_BYTE;
	/* Rx control */
	reg				curr__RX_RECV_EN, next__RX_RECV_EN;
	/* Rx data reg's */
	reg	[7:0]		curr__DAT_STATUS, next__DAT_STATUS;
	reg	[7:0]		curr__DAT_DX, next__DAT_DX;
	reg	[7:0]		curr__DAT_DY, next__DAT_DY;
	reg				curr__IRQ, next__IRQ;

	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			curr__state			<= 4'h0;
			curr__count			<= 24'h000000;
			curr__TX_XMIT		<= 1'b0;
			curr__TX_BYTE		<= 8'h00;
			curr__RX_RECV_EN	<= 1'b0;
			curr__DAT_STATUS	<= 8'h00;
			curr__DAT_DX		<= 8'h00;
			curr__DAT_DY		<= 8'h00;
			curr__IRQ			<= 1'b0;
		end
		else begin
			curr__state			<= next__state;
			curr__count			<= next__count;
			curr__TX_XMIT		<= next__TX_XMIT;
			curr__TX_BYTE		<= next__TX_BYTE;
			curr__RX_RECV_EN	<= next__RX_RECV_EN;
			curr__DAT_STATUS	<= next__DAT_STATUS;
			curr__DAT_DX		<= next__DAT_DX;
			curr__DAT_DY		<= next__DAT_DY;
			curr__IRQ			<= next__IRQ;
		end
	end

	/* next state ctrl */
	always @( * ) begin
		/* default values */
		next__state				= curr__state;
		next__count				= curr__count;
		next__TX_XMIT			= `FALSE;
		next__TX_BYTE			= curr__TX_BYTE;
		next__RX_RECV_EN		= `FALSE;
		next__DAT_STATUS		= curr__DAT_STATUS;
		next__DAT_DX			= curr__DAT_DX;
		next__DAT_DY			= curr__DAT_DY;
		next__IRQ				= `FALSE;

		case( curr__state )
			/* initial state - wait for 10ms before resetting the mouse */
			4'h0: begin
				if( curr__count == 5_000_000 ) begin /* 10ms @50MHz */
					next__state			= 4'h1;
					next__count			= 24'h000000;
				end
				else begin
					next__count			= curr__count + 1;
				end
			end

			/* start init by sending 0xFF */
			4'h1: begin
				next__state				= 4'h2;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= 8'hFF;
			end

			/* wait for confirmation that the byte has been sent */
			4'h2: begin
				if( TX_XMIT_OK ) begin
					next__state			= 4'h3;
				end
			end

			/* should recv 0xFA, if not, go back to reset */
			4'h3: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == 8'hFA ) & ( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= 4'h4;
					end
					else begin
						next__state		= 4'h0;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for self-test pass; if error, reset */
			4'h4: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == 8'hAA ) & ( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= 4'h5;
					end
					else begin
						next__state		= 4'h0;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for mouse ID byte. if error, reset */
			4'h5: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == 8'h00 ) & ( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		=4'h6;
					end
					else begin
						next__state		= 4'h0;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* send 0xF4 (enable reporting) */
			4'h6: begin
				next__state				= 4'h7;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= 8'hF4;
			end

			/* wait for confirmation of byte sent */
			4'h7: begin
				if( TX_XMIT_OK ) begin
					next__state			= 4'h8;
				end
			end

			/* if recv 0xFA, continue, else reset */
			4'h8: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == 8'hFA ) & ( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= 4'h9;
					end
					else begin
						next__state		= 4'h0;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* INIT SUCCESSFUL: Start reading data */
			/* wait for status byte */
			4'h9: begin
				if( RX_RECV_OK ) begin
					/* check for error in packet transmission */
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= 4'hA;
					next__DAT_STATUS	= RX_BYTE;
				end
				next__count				= 24'h000000;
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DX byte */
			4'hA: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= 4'hB;
					next__DAT_DX		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DY byte */
			4'hB: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= 4'hC;
					next__DAT_DY		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* IRQ time */
			4'hC: begin
				next__state				= 4'h9;
				/* only send an IRQ if the packet was intact */
				if( curr__count == 24'h000000 ) begin
					next__IRQ			= `TRUE;
				end
				next__count				= 24'h000000;
			end

			/* default state - should not be reachable */
			default: begin
				next__state				= 4'h0;
				next__count				= 24'h000000;
				next__TX_XMIT			= `FALSE;
				next__TX_BYTE			= 8'hFF;
				next__RX_RECV_EN		= `FALSE;
				next__DAT_STATUS		= 8'h00;
				next__DAT_DX			= 8'h00;
				next__DAT_DY			= 8'h00;
				next__IRQ				= `FALSE;
			end
		endcase
	end

	/* assign signals to ports */
	/* Tx side */
	assign TX_XMIT						= curr__TX_XMIT;
	assign TX_BYTE						= curr__TX_BYTE;
	/* Rx side */
	assign RX_RECV_EN					= curr__RX_RECV_EN;
	/* outputs */
	assign DAT_STATUS					= curr__DAT_STATUS;
	assign DAT_DX						= curr__DAT_DX;
	assign DAT_DY						= curr__DAT_DY;
	assign IRQ							= curr__IRQ;

endmodule
