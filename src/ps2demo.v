`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: 		UoE
// Engineer: 		Mihail Atanassov
// 
// Create Date:    	09:32:02 10/17/2012 
// Design Name: 	PS/2 Demo
// Module Name:    	ps2demo 
// Project Name: 	PS/2 Demo
// Target Devices: 	Digilent Basys2 100K
// Tool versions: 	Xilinx ISE WebPack 14.7 64-bit
//					Digilent JTAG Config Utility v2.1.1 Linux
// Description: 	A simple timer with start/stop switch and reset button.
//			Counts from 0.00s to 99.99s with 10ms precision.
// Dependencies: 	50MHz clock
//			GenericCounter.v -- n-bit up-counter with max value
//			Mux4.v -- 4-way 5-bit input mux
//			Seg7Decoder.v -- 7-segment display decoder module
// Revision: 
// 			Revision 2.00	-	Genvar for 4-bit counters;
//								2D wire arrays for counter o/ps.
//			Revision 1.00	-	Implementation complete.
//			Revision 0.01	-	File Created
// Additional Comments: 
//	16-bit and 2-bit counter do not reset in order to avoid display glitches
//		when resetting the timer.
//	Interface:
//		RESET:		Button input that resets the counters to 0.
//		CLK:		Clock input. 50MHz oscillator at M6.
//		SEG_SELECT:	Select line for the 7-segment display. Connected
//					to AN1 through to AN4.
//		DEC_OUT:	Data line for the 7-segment display. Connected
//					to CA through to CG and DP.
////////////////////////////////////////////////////////////////////////////////
module ps2demo
(
	input			CLK,
	input			RESET,
	/* PS/2 I/O */
	inout			IO_CLK,
	inout			IO_DAT,
	/* 7-seg o/p */
	output	[3:0]	SEG_SELECT,
	output	[7:0]	DEC_OUT,
	/* LEDs o/p */
	output	[7:0]	LED_OUT
);

/*----------------------------------------------------------------------------*\
 *	Wires used to connect the modules										  *
 *		Bit2Count:		2-bit counter output								  *
 *		Bit16TrigOut:		16-bit trigger output							  *
 *		MuxOut:			4-way mux output									  *
 *		DecCount:		4x 4-bit counter outputs. DecCount<0>				  *
 *						is unused.											  *
 *		DecCountAndDOT:		4x 5-bit multiplexor inputs						  *
 *		Bit4Trig:		4-bit counter triggers. Bit4Trig<5>					  *
 *						is unused.											  *
\*----------------------------------------------------------------------------*/
	wire	[1:0]	Bit2Count;
	wire			Bit16TrigOut;
	wire	[4:0]	MuxOut;
	wire [3:0] status;
	wire [7:0] xpos;
	wire [6:0] ypos;

/*----------------------------------------------------------------------------*\
 *	16-bit Counter : Trigger pulse every 1ms (@1kHz)						  *
 *		CLK:		Clock input. 50MHz oscillator on M6 used.				  *
 *		RESET:		Reset line. Always deasserted.							  *
 *		ENABLE_IN:	Enable signal. External control from switch.			  *
 *		TRIG_OUT:	Trigger for both 2-bit counter and 	 					  *
 *					first 4-bit counter.									  *
 *		COUNT:		Unused.													  *
\*----------------------------------------------------------------------------*/
	generic_counter
	#(
		.COUNTER_WIDTH(16),
		.COUNTER_MAX(49_999))
	Bit16
	(
		.CLK(CLK),
		.RESET(1'b0),
		.ENABLE_IN(1'b1),
		.TRIG_OUT(Bit16TrigOut),
		.COUNT(),
		.DIRECTION(1'b1)
	);

/*----------------------------------------------------------------------------*\
 *	2-bit Counter : Controls 4-way mux to output to 7-segment display		  *
 *		CLK:		Clock input. 50MHz oscillator on M6 used.				  *
 *		RESET:		Reset line. Always deasserted to avoid 					  *
 *					display glitches.										  *
 *		ENABLE_IN:	Enable line. Controlled from 16-bit counter.			  *
 *		TRIG_OUT:	Unused.													  *
 *		COUNT:		Controls the mux and 7-segment decoder					  *
\*----------------------------------------------------------------------------*/
	generic_counter
	#(
		.COUNTER_WIDTH(2),
		.COUNTER_MAX(3))
	Bit2
	(
		.CLK(CLK),
		.RESET(1'b0),
		.ENABLE_IN(Bit16TrigOut),
		.TRIG_OUT(),
		.COUNT(Bit2Count),
		.DIRECTION(1'b1)
	);


/*----------------------------------------------------------------------------*\
 *	4-way Mux. See diagram in logbook										  *
 *		S:	Select line														  *
 *		A:	Most significant digit i/p										  *
 *		B:	Digit 3 i/p														  *
 *		C:	Digit 2 i/p														  *
 *		D:	Least Significant digit i/p										  *
 *		OUT:	Output to the 7-segment decoder								  *
\*----------------------------------------------------------------------------*/
	mux4x5	mux
	(
		.S(Bit2Count),
		.D({1'b0,ypos[3:0]}),
		.C({2'b00,ypos[6:4]}),
		.B({1'b1,xpos[3:0]}),
		.A({1'b0,xpos[7:4]}),
		.OUT(MuxOut)
	);

/*----------------------------------------------------------------------------*\
 *	7-segment decoder. Determines which of the four digits of the display	  *
 *		to operate and decodes BNN values to 7-segment signals.				  *
 *		SEG_SELECT_IN:	Select line for which of the digits to				  *
 *					operate													  *
 *		BIN_IN:		Input data in BNN format.								  *
 *		DOT_IN:		Input bit for the decimal point segment.				  *
 *		SEG_SELECT_OUT:	4-bit positional code; selects one of the 4 		  *
 *					anodes of the display									  *
 *		HEX_OUT:	Display control signals. Determines the					  *
 *					symbol to be outputted.									  *
\*----------------------------------------------------------------------------*/
	seg7decoder	s7dec
	(
		.SEG_SELECT_IN(Bit2Count),
		.BIN_IN(MuxOut[3:0]),
		.DOT_IN(MuxOut[4]),
		.SEG_SELECT_OUT(SEG_SELECT),
		.HEX_OUT(DEC_OUT)
	);

	/* mouse driver instance */
	ps2mousedrv mouse
	(
		.CLK(CLK),
		.RESET(RESET),
		.IO_CLK(IO_CLK),
		.IO_DAT(IO_DAT),
		.DAT_STATUS(status),
		.DAT_X_POS(xpos),
		.DAT_Y_POS(ypos)
	);

	assign LED_OUT = {4'h0,status};

endmodule
