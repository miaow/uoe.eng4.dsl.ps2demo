`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: 		UoE
// Engineer: 		Mihail Atanassov
// 
// Create Date:    	09:59:44 10/17/2012 
// Design Name: 	GenericCounter
// Module Name:		GenericCounter 
// Project Name:	GenericCounter 
// Target Devices: 	any
// Tool versions: 	Xilinx ISE WebPack 14.2
// Description: 	A generic n-bit counter with a custom restart value
//
// Dependencies: 	none
//
// Revision: 
// 			Revision 1	-	Implementation complete
//			Revision 0.01	-	File Created
// Additional Comments: 
//	Parameters:
//	(4)	COUNTER_WIDTH:	bit-width of the counter. Determines max output
//	(9)	COUNTER_MAX:	Determines the value when the counter resets.
//	Interface:
//		CLK:		Clock input. 50MHz oscillator.
//		RESET:		Asynchronous reset input.
//		ENABLE_IN:	Enable input. If deasserted, counter will not
//					advance on CLK positive edge.
//		TRIG_OUT:	Trigger output. Asserted when COUNT is 0; 
//					deasserted otherwise.
//		COUNT:		Count output.
////////////////////////////////////////////////////////////////////////////////
module generic_counter(
		CLK,
		RESET,
		DIRECTION,
		ENABLE_IN,
		TRIG_OUT,
		COUNT
	);

	parameter					COUNTER_WIDTH = 4;
	parameter					COUNTER_MAX = 9;
	
	input 						CLK;
	input 						RESET;
	input						DIRECTION;
	input						ENABLE_IN;
	output						TRIG_OUT;
	output	[COUNTER_WIDTH-1:0]	COUNT;
	
	// Registers that hold the counter value and trigger value
	reg		[COUNTER_WIDTH-1:0]	Counter;
	reg							Trigger_Out;
	// Assign registers to outputs
	assign COUNT	= Counter;
	assign TRIG_OUT = Trigger_Out;
	
	// Counter logic
	always@(posedge CLK or posedge RESET) begin
		if(RESET) begin
			Counter <= 0;
		end
		else begin
			if(ENABLE_IN) begin
				if(DIRECTION) begin
					if(Counter == COUNTER_MAX) begin
						Counter <= 0;
					end
					else begin
						Counter <= Counter + 1;
					end
				end
				else begin
					if(Counter == 0) begin
						Counter <= COUNTER_MAX;
					end
					else begin
						Counter <= Counter - 1;
					end
				end
			end
		end
	end
	
	//Trigger logic
	always@(posedge CLK or posedge RESET) begin
		if(RESET) begin
			Trigger_Out <= 0;
		end
		else begin
			if(ENABLE_IN) begin
				if(Counter == COUNTER_MAX) begin
					Trigger_Out <= 1;
				end
				else begin
					Trigger_Out <= 0;
				end
			end
		end
	end

endmodule
