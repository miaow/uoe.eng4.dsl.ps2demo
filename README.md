PS/2 Demo
=========


This demo consists of a PS/2 transceiver with support for basic mode (three
buttons and X and Y directions) and Microsoft Intellimouse mode (three buttons
and X, Y, and Z directions). It does not support buttons 4 and 5.

## Project structure


The project's top module, `ps2demo.v`, contains drivers for the Basys2's LEDs
and seven-segment displays, as well as containing the actual mouse
transceiver. The transceiver itself (in `ps2mousedrv.v`) instantiates the
transmitter, the receiver, and the master state machine that controls them
both, and it keeps the current mouse X, Y, and Z positions. It also drives the
`CLK` and `DAT` I/O lines for communicating with the PS/2 mouse. The 
transmitter is located in `ps2tx.v`, the receiver - in `ps2rx.v`, and the
state machine - in `ps2ctrl.v`.

    +-------------------------------------------------+
    | ps2demo.v                                       |
    | +---------------------+ +---------------------+ |
    | | generic_counter<16> | | generic_counter<2>  | |
    | | # used as a 1kHz    >-> # used to control   | |
    | | # clock             | | # the 4-way mux to  | |
    | +---------------------+ | # the 7-seg display | |
    | +---------------------+ +-------\/------------+ |
    | | mux4x5              | +-------\/------------+ |
    | | # selects the data  | | seg7decoder         | |
    | | # to output to the  >-> # drives the four   | |
    | | # 7-seg display     | | # 7-seg digits      | |
    | +---------------------+ +---------------------+ |
    |                                                 |
    | +---------------------------------------------+ |
    | | ps2mousedrv                                 | |
    | | # the encompassing module for the PS/2      | |
    | | # driver                                    | |
    | | +-------------------+ +-------------------+ | |
    | | | ps2ctrl           | | ps2rx             | | |
    | | |                   >-> # the receiver    | | |
    | | |                   | |                   | | |
    | | |                   | +-----/ \--/ \------+ | |
    | | |                   |        |    L--O------+-+--- (IO_DAT)
    | | |                   |        L-O---->|<-----+-+--- (IO_CLK)
    | | |                   |          |     |      | |
    | | |                   | +-------\I/---/I\---+ | |
    | | |                   | | ps2tx             | | |
    | | |                   >-> # the transmitter | | |
    | | |                   | |                   | | |
    | | +-------------------+ +-------------------+ | |
    | +---------------------------------------------+ |
    |                                                 |
    +-----/ \--/ \--\[8]/--\[8]/--\[4]/--/ \----------+
           |    |     |      |      |     |
           |    |     |      |      |     L--------------- (BUTTON)
           |    |     |      |      L--------------------- (SEG_SELECT)
           |    |     |      L---------------------------- (DEC_OUT)
           |    |     L----------------------------------- (LED_OUT)
           |    L----------------------------------------- (CLK)
           L---------------------------------------------- (RESET)

## Input/Output Assignments


 - `RESET` is placed on `G12` (`BTN0`);
 - `EN_SCROLL` is placed on `P11` (`SW0`);
 - X position is placed on digits 3 and 2 of the seven-segment display;
 - Y position is placed on digits 1 and 0 of the seven-segment display;
 - Z position (if enabled) is placed on LEDs 7 through 4 (`G1`, `P4`, `N4`,
`N5`);
 - The status bits are placed on LEDs 3 through 0 (`P6`, `P7`, `M11`, `M5`);
 - `CLK` is sourced from the external socket on `M6`. There's an alternative
line in the UCF if an external clock source is not available.

### Testing performed

During the development of the mouse driver, the primary means of debugging the
communications was the use of Chipscope. It showed, at first, a complete lack of
parity between specifications and implementation. After some errors were found,
the PS/2 mouse data was successfully captured using Chipscope, even though the
demo was still inoperable (see commit d0b9cb1). Again using Chipscope, all the
outstanding errors were fixed and the PS/2 driver was complete. Later, I created
a few testbenches that monitor the RX and TX modules, and a few minor details
popped up that needed to be fixed (e.g. uninitialised output buffer, which is
not a problem on the FPGA as all the values are set to zero; however, if taped
out to an ASIC, this could lead to unexpected behaviour after power-on and after
reset).

### Folder Structure

 - `ipcore_dir`: contains the Chipscope generated IP core;
 - `iseconfig`: contains some ISE config files, pretty much unnecessary for
opening and syntesizing the project;
 - `src`: all the source files (for implementation) are located here;
 - `test`: all testbenches and their wave config files are located here;
 - `workdir`: this folder is used by ISE to store temporary and intermediate
files.
